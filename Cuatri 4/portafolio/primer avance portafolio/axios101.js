// Librería Axios 
const axios = require("axios")

// URL de la API JSONPlaceholder 
const url = "https://jsonplaceholder.typicode.com/users"

//Se hace una solicitud GET a la API JSONPlaceholder e imprime el nombre de usuario

axios.get(url).then(response => {
    response.data.forEach(element =>{
    console.log(element.username)
  });
 })

// Se hace una solicitud POST a la API JSONPlaceholder
// La respuesta se imprime 

axios.post(url, {
    username: "Foo Bar",
    email: "Foo bar"
}).then(response => console.log(response.data))

