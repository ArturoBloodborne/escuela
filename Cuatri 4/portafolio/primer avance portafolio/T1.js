//Imprimir mensaje en el navegador
console.log("Hola Mundo XD")


// Declaración de variables 
//  s es una variable global
//  x tiene alcance de bloque
//  y es una variable global

    var s = "Foo Bar"

    let x = 90
    var y = 89

    // Declaración del Arreglo(array) con diferentes tipos de datos
    array = [1, 2, 3, 4, 5, "Foo", "Bar", true, false, 2.34, 4.23]
// Imprimir array
    console.log(array)

    // Seleccion del indice 5 del array
    console.log(array[5])
// Declaración del objeto con propiedades y un array anidado para el autollamado
    var obj = {
    first_name: "Foo",
     last_name: "Bar",
      age: 23,
       city: "tj",
        status: true,
         arr: array}

    // Imprime el objeto y sus propiedades
    console.log(obj)
    console.log(obj["first_name"])
    console.log(obj.last_name)

    // Bucle for que imprime números del 0 al 99
    for(let i=0; i<100; i+=5) {
        console.log(i)
    }

    // Bucle for que imprime cada elemento del array
    for (let i=0; i< array.length; i++){
        console.log(array[i])
    }

    // Forma mas rapida y sencilla de imprimir el array
    for(let i of array){
        console.log(i)
    }

    // utiliza un bucle 'for of' para recorrer los valores clave del objeto e imprimirlos

    for(let key of Object.keys(obj)){
        console.log(key + ": " + obj[key])
    }

    // bucle 'for in' recorre las propiedades del objeto e imprime sus valores

    for (let key in obj) {
        console.log(key + ": " + obj[key])
    }
    // Bucle while que multiplica i por 5 hasta que sea mayor o igual a 1000
    var i = 0
    while (i < 1000){
        console.log(i)
        i+=5
    }
    /* Bucle do while que imprime el valor de k y luego incrementa k en 5 
    hasta que k sea mayor o igual a 1000 */
    var k = 0
    do {
        console.log(k)
        k+=5
    }while(k < 1000)

    // declaracion de variables
    var z = 9
    var y = 8

    /* evaluacion de las variables y 
    despliega tal mensaje dependiendo el resultado de la evaluacion */
    if(z > y){
        console.log("is it")
    }else{
        console.log("is Not")
    }

    // declaracion de variable
    var animal = "kitty"

    /* Se utiliza el operador para determinar si la variable
    es igual a Kitty y asi asignar el mensaje correspondiente */

    var hello = (animal==="kitty") ? "It is a pretty Kitty" : "Is Not a pretty Kitty"


    // declaracion de la variable
    var opc = 8

    /*Se usa una evaluacion switch para determinar el valor 
    de la variable e imprimir los mensajes segen el caso*/

    switch(opc) {
        case 1:
            console.log("I am Case 1")
            break;
        case 8:
            console.log("I am IronMan")
            break;
        default:
            console.log("I am Inevitable")
            break;
    }


    // se declaran las variables
    var prism = function (l, w, h) {
      return  l * w * h
    }

    // Función de cálculo del volumen de un prisma
    console.log("Volumen del Prisma " + prism(23, 56, 12))

    function prisma(l) {
      return function(w) {
        return function(h) {
          return l * w * h
        }
      }
    }

    console.log("Volumen del Prisma " + prisma(23)(12)(56))

  
    // Declaración de una función con nombre y una función anónima
    var namedSum = function (a, b) { 
      return a + b;
      }((3, 2));

      console.log(namedSum)

      var anonSum = function(a, b) {
        return a + b;
     };
     console.log(anonSum(5, 5));
    
     
     /*Funcion recursiva say que imprime "hello"
      un numero determinado de veces.*/

    var say = function say(times) {
      if (times > 0) {
        console.log("hello")
        say(times - 1)
      }
    }

    var saysay = say

    say = "oops"

    saysay(5)

    // Llamada a una API usando fetch para obtener datos de Stack Exchange y JSONPlaceholder
    const url = 'http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript';

    var questionList = document.createElement('ul');
    document.body.appendchild(questionList);

    var responseData = fetch(url).then(response => response.json());
    responseData.then(({items, has_more, quota_max,}) => {
      for(var {title, score, owner, link, answer_count} of items) {
        var listItem = document.createElement('li');
        questionList.appendchild(listItem);
        var a = document.createElement('a');
        listItem.appendchild(a);
        a.href = link;
        a.textContent = `[${score}] ${title} (by ${owner.display_name || 'somebody'})`
      }
    }
    );

    fetch(url, {
      method: "POST",
      headers: {
        "content_type": "application/json"
      },
      body: JSON.stringify({
        username: "Foo Bar",
        email: "Foo@bqr.com"
      })
    }).then(response => response.json())
      .then(response => console.log(response))