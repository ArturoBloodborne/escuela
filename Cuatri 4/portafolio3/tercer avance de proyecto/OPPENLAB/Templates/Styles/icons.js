const Home = require("../../assets/icon/Home.png");
const Settings = require("../../assets/icon/configuraciones.png");
const Devices = require("../../assets/icon/devices.png");
const Menu = require("../../assets/icon/menu.png");
const Notifications = require("../../assets/icon/notifications.png");
const Close = require("../../assets/icon/cerrar.png");
const Process = require("../../assets/icon/process.png");
const Factory = require("../../assets/icon/industria.png");
const Task = require("../../assets/icon/task.png");
const Avatar = require("../../assets/icon/avatar.png");
const Employees = require("../../assets/icon/personas.png");
const Storage = require("../../assets/icon/almacen.png");
const dot3 = require("../../assets/icon/mas.png");
const humedad = require("../../assets/icon/humedad.png");
const temperatura = require("../../assets/icon/celsius.png");
const Mas = require("../../assets/icon/suma.png");
const Menos = require("../../assets/icon/menos.png");
const Back = require("../../assets/icon/Back.png");
const NoWifi = require("../../assets/icon/no-wifi.png");
const Logo = require("../../assets/icon/Logo.png");
const Pendiente = require("../../assets/icon/portapapeles.png");

export default {
  Home,
  Settings,
  Devices,
  Menu,
  Notifications,
  Close,
  Process,
  Factory,
  Task,
  Avatar,
  Employees,
  Storage,
  dot3,
  humedad,
  temperatura,
  Mas,
  Menos,
  Back,
  NoWifi,
  Logo,
  Pendiente,
};
