import React from 'react';
import { CardTouch, Media, CardContent, Wrap, InfoTitle, SubInfo, Horizontal, Carousel,H2, ErrorCard } from '../../Root/Main';
import * as Style from '../../../Styles/Style';
import { View } from 'react-native';
import { BtnIconText } from '../Tools/BtnIconText';

function isEmpty(item=[]) {
    if (item.length == 0) {
        return true;
    }
    return false;
    }

export const InfoCarousel = ({list}) => {
    if (isEmpty(list)) {
        return (
          <ErrorCard style={Style.InfoCard.vista}></ErrorCard>
        );
      }
  
      try{
        return (
            <View>
                <Carousel items={list} styleItem={Style.CarouselInfo.static}
                renderItem={({item, style}) => {
                    return (
                        <CardTouch key={item.numero} style={[Style.InfoCard, Style.InfoCard.horizontal,{marginBottom: 0,}, style]}>
                            <Horizontal>
                                <Media source={item.url}/>
                                <CardContent>
                                    <InfoTitle >{item.nombre}</InfoTitle>
                                    <SubInfo>{item.areaProduccion}</SubInfo>
                                </CardContent>
                            </Horizontal>
                        </CardTouch>
                    );
                }}/>
                <H2 style={{alignSelf: 'center'}}>Jefes de Área</H2>
            </View>
          );
      }catch{
        return (
          <ErrorCard style={Style.InfoCard.vista}></ErrorCard>
        );
      }
};

