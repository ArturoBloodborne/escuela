import { View } from "react-native"
import { horizontal } from "../../Styles/Style"

const Horizontal = ({children, style}) => {
    return (
        <View style={[horizontal, style]}>
            {children}
        </View>
    )
}
export default Horizontal;