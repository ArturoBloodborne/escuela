import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import * as Style from '../../Styles/Style.js';

export const Card = ({children, style}) => {
  return (
    <View style={[Style.card, style]}>
      <View style={Style.container}>{children}</View>
    </View>
  );
};

export const CardTouch = ({children, style, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[Style.card, style]}>
      <View style={Style.container}>{children}</View>
    </TouchableOpacity>
  );
};

export const CardContent = ({children, style}) => {
  return <View style={[Style.content, style]}><View>{children}</View></View>;
};




