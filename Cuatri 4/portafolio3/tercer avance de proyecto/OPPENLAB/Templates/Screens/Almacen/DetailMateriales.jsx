import { Body, TitleBackCard, TitleCard} from '../../Components/Mix/Main';
import { BigAvatar, Card, Center, Title, Wrap, } from '../../Components/Root/Main';
import { ScrollView } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Style from '../../Styles/Style';
import { GETnumero } from '../../../Core/Connection/Global';
import * as Theme from '../../Styles/Theme';
import { View } from 'react-native-animatable';

const DetailMaterialScreen = ({ route}) => {
    const Navigate = Screens();
    const { material } = route.params;
    //const detail = GETnumero('detailMaterial/', material.numero);

    // Verificar si la data está disponible y si contiene el índice "numero"
    //const detailData = detail && detail.length > 0 && detail[0].numero === material.numero ? detail[0] : null;

    return (
        <Body style={{backgroundColor: Theme.color.subFondo }}>
            <TitleBackCard onPress={Navigate.Back}>Detalles</TitleBackCard>
            <ScrollView>
                <Wrap>
                    <Card style={[Style.PresentationCard, { height: 250 }]}>
                        <Center>
                            <BigAvatar url={material ? material.url : ""} />
                        </Center>
                    </Card>
                    <Title style={{ color: Theme.color.texto.normal, alignSelf: "flex-start" }}>
                        Nombre: 
                    </Title>
                    <TitleCard 
                    style={{height:50}}

                    styleText={{
                        fontSize: 22 
                    }}>
                        {material ? material.nombre : ""}
                    </TitleCard>                
                </Wrap>
                <Title style={{color: Theme.color.texto.normal,alignSelf: "center"}}>
                        Stock:
                    </Title>

                    <Card style={[Style.TopCard, {
                            marginBottom: 0,
                            backgroundColor: Theme.color.fourth, 
                            justifyContent: 'space-between', 
                            height: 70, 
                            width: 170,
                            flex: 1,
                            borderWidth: 0,
                            borderColor: 'transparent',
                            elevation: 0,
                            margin: 0,
                            borderRadius: 12,
                            alignItems: 'center',
                            alignSelf: 'center',
                            
                            }]}>
                        <Title style={{color: Theme.color.texto.normal, alignSelf: "center", fontSize: 22, marginTop: 20}}>
                            {material ? material.stock : ""}
                        </Title>
                    </Card>
            </ScrollView>
        </Body>
    );
}

export default DetailMaterialScreen;