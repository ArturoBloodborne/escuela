import { Body, SubTitle, CarouselCard, TitleBackCard, InfoCard } from '../../Components/Mix/Main'
import { InfoCardPiezas } from '../../Components/Mix/Home/InfoCard'
import { ScrollView, Image } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { BigAvatar, Card, Button, Center, Title, H2, H1, Wrap } from '../../Components/Root/Main';
import * as Style from '../../Styles/Style';
import { GET } from '../../../Core/Connection/Global';




const PiezasScreen = () => {       //AQUI
    const Navigate = Screens();
    const Piezas = GET('piezas');

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Almacen</TitleBackCard>
            <ScrollView>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                <Title style={{marginBottom:10}}>Piezas</Title>
                <Title style={{marginBottom:10,alignSelf: "flex-start"}} >Aqui puedes encontrar todas las piezas que necesitas para fabricar los modelos.</Title>
                        
            </Card>
            <InfoCardPiezas style={{width: 150}} ImgStyle={{flex: 0, height: "50%"}} list={Piezas}/>

              
            
            </ScrollView>
        </Body>
    )
}

export default PiezasScreen;