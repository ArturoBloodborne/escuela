import { Body, TitleBackCard, InfoCard, SubInfo } from '../../Components/Mix/Main'
import { ScrollView, View } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { BigAvatar, Card, Center, Title, H2, H1, SubTitle, Wrap } from '../../Components/Root/Main';
import * as Style from '../../Styles/Style';
import Animated, { BounceOutUp,BounceInUp, } from 'react-native-reanimated';
import { GETuser } from "../../../Core/Connection/Global.js";


const PerfilScreen = () => {
    const Navigate = Screens();
    const detail = GETuser('perfil/');
    const detailData = detail && detail.length > 0  ? detail[0] : null;
    return (

        <Body animationIn={BounceInUp} animationOut={BounceOutUp}>
            <TitleBackCard onPress={ Navigate.Back }>Perfil</TitleBackCard>

            <ScrollView>
                <Wrap>
                <Card style={Style.GrandCard}>
                    <Center>
                        <BigAvatar url={'https://static.thenounproject.com/png/11404-200.png'}></BigAvatar>
                        <Title>{detailData?detailData.nombre:'-----'}</Title>
                        <H1 style={{fontSize: 11,marginBottom:70}}>{detailData? detailData.correo : "---"}</H1>
                    </Center>
                </Card>          
                    <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 250}]}>
                        <Title style={{marginBottom:10}}>Información de Trabajo</Title>
                        <Title style={{marginBottom:10,alignSelf: "flex-start"}} >Puesto: <SubInfo>{detailData ? detailData.puesto: '---'}</SubInfo></Title>
                        <Title style={{marginBottom:10, alignSelf: "flex-start"}} >Área de Producción: <SubInfo>{detailData? detailData.areaProduccion : '---'}</SubInfo></Title>
                        <Title style={{marginBottom:10, alignSelf: "flex-start"}} >Jefe de Área: <SubInfo>{detailData && detailData.jefeArea != null? detailData.jefeArea: 'Sin jefe'}</SubInfo></Title>
                        <Title style={{marginBottom:10, alignSelf: "flex-start"}} >Horario: <SubInfo>{detailData ? detailData.horario : '---'}</SubInfo></Title>
                    </Card>

                    <Card style={[Style.TopCard, {justifyContent: 'space-between', height: 250}]}>
                        <Title style={{marginBottom:20}}>Informacion Personal</Title>
                        <Title style={{marginBottom:20,alignSelf: "flex-start"}} >Número de Teléfono: <SubInfo>{detailData?detailData.telefono:'---'}</SubInfo></Title>
                        <Title style={{alignSelf: "flex-start"}} >Dirección: <SubInfo>{detailData?detailData.direccion:'---'}</SubInfo></Title>
                    </Card>
                
                </Wrap>
            </ScrollView>
        </Body>
    )
}

export default PerfilScreen;