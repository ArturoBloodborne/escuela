import { Body, TitleBackCard, H1, H2, Center, Title, Card, CarouselCard, SubTitle, AccordionItem, SubInfo, InfoUserArea} from '../../Components/Mix/Main'
import { ScrollView, Text, TextInput, View, Alert, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import React, { useState } from 'react';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Theme from '../../Styles/Theme';
import * as Style from '../../Styles/Style';
import { getUser as usuario } from '../../../Core/Data/User';
import { GETuser, POST } from '../../../Core/Connection/Global';



const AsignarTareas = () => {
    const user = 5;
    const area = GETuser('userArea/');
    const Navigate = Screens();
    const local = 5;
    const [estado, setEstado] = useState(1);

    
    const [descripcion, setdescripcion] = useState('');

    function pressButton() {
        try {
            POST('tareas', {
                "descripcion": descripcion,
                "estado": estado,
                "empleado": user,
                "area": local,
            });
    
            Alert.alert('Tarea asignada');
        } catch (error) {
            console.log(error);
            Alert.alert('Error', error.message);
        }
    }

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Nueva Tarea</TitleBackCard>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: "60%"}]}>
            <SubTitle style={{color: "white"}}>Descripción:</SubTitle>
                <Card style={{backgroundColor: "#FFFFFF", borderRadius: 25, width: "95%"}}>
                    <TextInput placeholder='Descripción de la nueva tarea.' value={descripcion} onChangeText={setdescripcion} placeholderTextColor={'gray'} />
                </Card>
                <TouchableOpacity style={{backgroundColor: Theme.color.tertiary}} className=" p-3 w-full rounded-2xl mt-5 mb-5" onPress={pressButton}>
                  <Text className="text-xl font-bold text-center text-white">Asignar tarea</Text>
                </TouchableOpacity>
                </Card>
                <InfoUserArea list={area}/>
        </Body>
    )
}


export default AsignarTareas;