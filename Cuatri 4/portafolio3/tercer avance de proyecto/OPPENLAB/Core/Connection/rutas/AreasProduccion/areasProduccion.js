import  * as Token  from "../../../Data/Token";
import { useEffect, useState } from "react";
import { URL } from "../../Global";

const extension = 'areasProduccion';
const endpoint = `${URL}${extension}`;

export const Areas = () => {
    const [areasData, setAreasData] = useState([]);

    useEffect(() => {
        const fetchAreas = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const data = await response.json();
                setAreasData(data);
            } catch {
                setAreasData([]);            
            }
        };

        fetchAreas();
    }, []);

    return areasData;
};