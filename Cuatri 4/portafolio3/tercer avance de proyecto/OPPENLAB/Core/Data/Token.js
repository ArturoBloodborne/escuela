import AsyncStorage from '@react-native-async-storage/async-storage';
//Propiedades del token estilo clase con metodos

//---Forma de usar los metodos---
// Token.getToken().then((token) => { console.log('Token:', token) });

// Token.setToken(data.token);

// Token.removeToken();
//-----------------------------

//Metodo de extraccion del Token
export async function getToken() {
    try {
        const token = await AsyncStorage.getItem('authToken');
        return token;
    } catch (error) {
        console.error('Error al recuperar el token:', error.message);
        return null;
    }
  }

//Metodo de creacion del Token
export async function setToken(token) {
    try {
        await AsyncStorage.setItem('authToken', token);
    } catch (error) {
        console.error('Error al crear el token:', error.message);
    }
    
}

//Metodo de Eliminacion del Token
export async function removeToken() {
    try {
        await AsyncStorage.removeItem('authToken');
    } catch (error) {
        console.error('Error al eliminar el token:', error.message);
    }    
}
