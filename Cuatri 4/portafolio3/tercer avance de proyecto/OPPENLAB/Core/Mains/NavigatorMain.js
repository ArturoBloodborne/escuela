import { useNavigation } from '@react-navigation/native';
import { AuthState } from '../Connection/Authenticator';

export const Screens = () => {
  try{
    //Creacion de la variable de navegacion
    const navigation = useNavigation();

    //Ruta de navegacion para Login
    const Login = () => {
      navigation.navigate('Login');
    };
      
    //Ruta de navegacion para El main tab
    const Tab = async() => {
      //Se usa AuthState para verificar el estado del token
      await AuthState() == true? 
        navigation.navigate('Tab')
        :navigation.navigate('Login');
    };

    //Ruta de navegacion para ForgotPassword
    const ForgotPassword = () => {
      navigation.navigate('ForgotPassword');
    };

    
    //Ruta de navegacion para ForgotPassword
    const Codigo = () => {
      navigation.navigate('Codigo');
    };
    
    //Ruta de navegacion para ForgotPassword
    const Password = () => {
      navigation.navigate('Password');
    };

    //Ruta de navegacion para Loading
    const Loading = () => {
      navigation.navigate('Loading');
    };

    //Ruta de navegacion para Perfil
    const Perfil = () => {
      navigation.navigate('Perfil');
    };

    //Ruta de navegacion para Tools
    const Tools = () => {
      navigation.navigate('Tools');
    };

    //Ruta de navegacion para Dispositivos
    const Devices = () => {
      navigation.navigate('Devices');
    };

    //Ruta de navegacion para Home
    const Home = () => {
      navigation.navigate('Home');
    };

    //Ruta de navegacion para Empleados
    const Empleados = () => {
      navigation.navigate('Empleados');
    };    

    //Ruta de navegacion para Area de Empleados
    const EmpleadosArea = (item) => {
      navigation.navigate('EmpleadosArea', {area: item});
    }   

    //Ruta de navegacion para Produccion
    const Produccion = () => {
      navigation.navigate('Produccion');
    };

    //Ruta de navegacion para Tareas
    const Tareas = () => {
      navigation.navigate('Tareas');
    }

    //Ruta de navegacion para Tareas
    const AsignarTarea = () => {
      navigation.navigate('Asignar');
    }

    //Ruta de navegacion para Reportar
    const Reportar = (item) => {
      navigation.navigate('Reportar', {tarea: item});
    }

    //Ruta de navegacion para Area de Tareas
    const TareasArea = (item) => {
      navigation.navigate('TareasArea', {area: item});
    }   

    //Ruta de navegacion para Procesos
    const Procesos = () => {
      navigation.navigate('Procesos');
    };

    //Ruta de navegacion para Almacen
    const Almacen = () => {
      navigation.navigate('Almacen');
    };

     //Ruta de navegacion para Modelos
     const Modelos = () => {
      navigation.navigate('Modelos');
    }; 

    //Ruta de navegacion para Modelos
     const DetailModelos = (item) => {
      navigation.navigate('DetailModelos', {modelo: item});
    };                                    

    const Back = () => {
      navigation.goBack();
    }

    //Ruta de navegacion para Modelos
     const Piezas = () => {
      navigation.navigate('Piezas');
    };

    //Ruta de navegacion para Modelos
    const DetailPieza = (item) => {
      navigation.navigate('DetailPieza', {pieza: item});
    };

      //Ruta de navegacion para Modelos
     const Materiales = () => {
      navigation.navigate('Materiales');
    }; 
    
    //Ruta de navegacion para Modelos
    const DetailMaterial = (item) => {
      navigation.navigate('DetailMaterial', {material: item});
    };

    //Retorno de las rutas de navegacion
    return {
      Login,
      Home,
      ForgotPassword,
      Password,
      Codigo,
      Loading,
      Perfil,
      Tools,
      Devices,
      Tab,
      Empleados,
      EmpleadosArea,
      Produccion,
      Tareas,
      TareasArea,
      AsignarTarea,
      Procesos,
      Almacen,
      Modelos,
      DetailModelos,
      Piezas,
      DetailPieza,
      Materiales,
      DetailMaterial,
      Back,
      Reportar,
    };
  }catch(error){
    console.log(error);
  }
  
};