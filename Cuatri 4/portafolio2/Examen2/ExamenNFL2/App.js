import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function App() {
  const [todos, setTodos] = useState([]);
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(data => setTodos(data))
      .catch(error => console.error('Error fetching todos:', error));
  }, []);

  const getTodoIDs = () => {
    return todos.map(todo => ({ id: todo.id }));
  };

  const getTodoIDAndTitles = () => {
    return todos.map(todo => ({ id: todo.id, title: todo.title }));
  };

  const getUnresolvedTodos = () => {
    return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
  };

  const getResolvedTodos = () => {
    return todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
  };

  const getTodoIDsAndUserID = () => {
    return todos.map(todo => ({ id: todo.id, userId: todo.userId }));
  };

  const getResolvedTodosAndUserID = () => {
    return todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
  };

  const getUnresolvedTodosAndUserID = () => {
    return todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
  };

  const filterTodos = (filterFunction) => {
    setFilteredTodos(filterFunction());
  };

  return (
    <View style={styles.container}>
      <View style={styles.menu}>
        <Button title="Todos los pendientes (solo IDs)" onPress={() => filterTodos(getTodoIDs)} />
        <Button title="Todos los pendientes (IDs y Titles)" onPress={() => filterTodos(getTodoIDAndTitles)} />
        <Button title="Todos los pendientes sin resolver (ID y Title)" onPress={() => filterTodos(getUnresolvedTodos)} />
        <Button title="Todos los pendientes resueltos (ID y Title)" onPress={() => filterTodos(getResolvedTodos)} />
        <Button title="Todos los pendientes (IDs y userID)" onPress={() => filterTodos(getTodoIDsAndUserID)} />
        <Button title="Todos los pendientes resueltos (ID y userD)" onPress={() => filterTodos(getResolvedTodosAndUserID)} />
        <Button title="Todos los pendientes sin resolver (ID y userID)" onPress={() => filterTodos(getUnresolvedTodosAndUserID)} />
      </View>
      <View style={styles.results}>
        {filteredTodos.map(todo => (
          <Text key={todo.id}>{JSON.stringify(todo)}</Text>
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menu: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginBottom: 20,
  },
  results: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },
});
