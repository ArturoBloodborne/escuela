const express = require('express');
const piezas = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

piezas.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM piezas', (err, rows) => {
            if(err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

piezas.post('/', (req, res) => {
    req.getConnection((err, connection) => {

        connection.query('INSERT INTO piezas SET ?', [req.body], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se agrego exitosamente')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

piezas.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM piezas WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

piezas.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE piezas SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = piezas