import {
Box, ButtonText, VStack, HStack, Button, Text, View, Avatar, AvatarImage, Center,
Actionsheet, ActionsheetBackdrop, ActionsheetContent, ActionsheetItem, ActionsheetItemText,
ActionsheetDragIndicatorWrapper, ActionsheetDragIndicator 
} from '@gluestack-ui/themed';
import React from 'react';
import {  StyleSheet, Dimensions } from "react-native";
import { CubeButton, Title, Footer, Screens  } from "../Components/Main";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const screenWidth = Dimensions.get('window').width;

const Tools = () => {
  const screen = Screens();
  const handleClose = () => setShowActionsheet(!showActionsheet);
  const [showActionsheet, setShowActionsheet] = React.useState(false);

  const Menu = () => {
    return (
      <Box>
        <Actionsheet isOpen={showActionsheet} onClose={handleClose} zIndex={999}>
          <ActionsheetBackdrop />
          <ActionsheetContent h='$1/3' zIndex={999}>
            <ActionsheetDragIndicatorWrapper>
              <ActionsheetDragIndicator />
            </ActionsheetDragIndicatorWrapper>
            <ActionsheetItem onPress={handleClose}>
              <ActionsheetItemText>Investigación y Desarrollo (I+D)</ActionsheetItemText>
            </ActionsheetItem>
            <ActionsheetItem onPress={handleClose}>
              <ActionsheetItemText>Síntesis Química</ActionsheetItemText>
            </ActionsheetItem>
            <ActionsheetItem onPress={handleClose}>
              <ActionsheetItemText>Formulación</ActionsheetItemText>
            </ActionsheetItem>
            <ActionsheetItem onPress={handleClose}>
              <ActionsheetItemText>Cancel</ActionsheetItemText>
            </ActionsheetItem>
          </ActionsheetContent>
        </Actionsheet>
      </Box>
  );
  }

  return (
    <View style={styles.container}>
      <Menu/>
      <View>
        <View>
          <Box
            bg="#2D3154"
            py="$3"
            px="$3"
            height={170}
            width="$full"
          >
            <VStack $ios-pt={30} justifyContent='space-between' w="$full" height="$full" space="$xl">
              
            
                <HStack alignSelf='flex-end' ml={14}>
                  <Button borderRadius={25} action="secondary" variant='outline' bg='transparent'  m={2}  size="xs" >
                  <MaterialCommunityIcons name="bell" color="#f0edf6" size={20} /> 
                  </Button>
                  <Button borderRadius={25} action="secondary" variant='outline' bg='transparent' m={2} size="xs">
                  <MaterialCommunityIcons name="view-headline" color="#f0edf6" size={20} />
                  </Button>
                </HStack>
                          
                  <Button $ios-mb={15} mb={30} alignSelf='flex-start' bg='transparent' >
                      <HStack alignItems="center">
                        <Avatar alignSelf='center' size='lg'>
                          <AvatarImage alt='si' source={require('../assets/user.jpg')} />  
                        </Avatar>
                        <VStack ml="$2">
                            <Text  color="#FFFFFF">
                              Usuario
                            </Text>
                            <Text color="#FFFFFF" >
                              @Usuario
                            </Text>
                        </VStack>
                      </HStack>
                  </Button>
                                
                  <Button alignSelf='flex-end' borderRadius={25} bgColor='#4B7583' size="xs" $active-bg='#4B7563'>
                  <MaterialCommunityIcons name="account-circle-outline" color="#FFFFFF" size={20} />
                    <ButtonText ml={4}
                    
                      alignItems='center'
                      textTransform="uppercase"
                      fontSize="$sm"
                      fontWeight="$bold"
                      color="$white"
                    >
                      Ver Perfil
                    </ButtonText>
                  </Button>        
            </VStack>
          </Box>
        </View>
        <Title text="Herramientas"/>
        <Center w="$full" h="$full"  >
          <Box w="$full" h="$full" maxWidth={600} >
            
            <VStack space="2xl" >
              <HStack w="$full" justifyContent='space-between'>
                <CubeButton text="Empleados" Icon='account-group-outline' onPress={screen.Empleados}/>
                <CubeButton text="Áreas de" text2="Producción" Icon='factory'/>
                <CubeButton text="Asignar" text2="Tareas" Icon='clipboard-text-outline' onPress={handleClose}/>
                
              </HStack>

              <HStack w="$full" justifyContent='space-between'>
                <CubeButton text="Procesos" Icon='cogs'/>
                <CubeButton text="Almacén" Icon='warehouse' Screen='Almacen' onPress={screen.Almacen}/> 
                <CubeButton text="Otros" Icon='dots-horizontal'/>
              </HStack>
            </VStack>
          </Box>             
        </Center>
      </View>
      <Footer/>
    </View>
  );
}




const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    // paddingLeft: 15,
    // paddingRight: 15,

    justifyContent: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
    // paddingLeft: 15,
    // paddingRight: 15,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});

export default Tools;

