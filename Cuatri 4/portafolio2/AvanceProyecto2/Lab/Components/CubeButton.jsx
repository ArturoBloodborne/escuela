import { Text, Box, Button } from '@gluestack-ui/themed';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

export const CubeButton = ({ text, text2 = " ", Icon="", onPress }) => {

    function res() {
        if (screenWidth < 326 ) {
            return {
                w: 70,
                h: 70,
                s: 40
            }
        }else if (screenWidth < 500) {
            return {
                w: 100,
                h: 100,
                s: 45,
            }
        }{
            return {
                w: 120,
                h: 120,
                s: 65,
            }
        }
    }
    
    return(
        <Box w={res().w} h={res().h} alignItems='center' m={10} p="$2" >
            <Button w="$full" h="$full" bg='#6D4B70' $active-bg='#2D3154'  borderRadius={5} onPress={onPress}>
                <MaterialCommunityIcons name={Icon} color="#FFFFFF" size={res().s} />
            </Button>
            <Text fontSize={11} color='#3B1B0D' textAlign='justify'>{text}</Text>
            <Text fontSize={11} color='#3B1B0D' textAlign='justify'>{text2}</Text>
        </Box>
    );   
}


