import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import SignIn from './SignIn';
import SignUp from './SignUp';
import { getAuth, onAuthStateChanged, signOut } from 'firebase/auth';
import { initializeApp } from 'firebase/app';
import { StatusBar } from 'expo-status-bar';

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyAbd05pfbzkRjh1jn3ImAfxCv4ls2DwkNk",
  authDomain: "proy1-e55ec.firebaseapp.com",
  projectId: "proy1-e55ec",
  storageBucket: "proy1-e55ec.appspot.com",
  messagingSenderId: "696029978322",
  appId: "1:696029978322:web:d6a0cf774626d55bf1170b"
};

// Inicializa Firebase
const app = initializeApp(firebaseConfig);

const AuthenticatedScreen = ({ user, handleLogout }) => (
  <View style={styles.authContainer}>
    <Text style={styles.title}>Welcome!</Text>
    <Text style={styles.emailText}>{user.email}</Text>
    <Button title="Logout" onPress={handleLogout} color="#e74c3c" />
  </View>
);

const App = () => {
  const [user, setUser] = useState(null);
  const [isLogin, setIsLogin] = useState(true);

  const auth = getAuth(app);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setUser(user);
    });

    return () => unsubscribe();
  }, [auth]);

  const handleLogout = async () => {
    try {
      await signOut(auth);
    } catch (error) {
      console.error('Logout error:', error.message);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar style="auto" />
      {user ? (
        <AuthenticatedScreen user={user} handleLogout={handleLogout} />
      ) : (
        isLogin ? (
          <SignIn />
        ) : (
          <SignUp />
        )
      )}
      <Text style={styles.toggleText} onPress={() => setIsLogin(!isLogin)}>
        {isLogin ? 'Need an account? Sign Up' : 'Already have an account? Sign In'}
      </Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#f0f0f0',
  },
  authContainer: {
    width: '80%',
    maxWidth: 400,
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 8,
    elevation: 3,
  },
  title: {
    fontSize: 24,
    marginBottom: 16,
    textAlign: 'center',
  },
  emailText: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 20,
  },
  toggleText: {
    color: '#3498db',
    textAlign: 'center',
    marginTop: 20,
  },
});

export default App;
