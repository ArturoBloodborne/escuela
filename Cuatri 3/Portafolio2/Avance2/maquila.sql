-- Active: 1699132662780@@127.0.0.1@3307@maquila
CREATE DATABASE maquila;

-- COMANDOS DE USO CONCURRENTE:
use maquila;
SELECT * FROM ;
DROP TABLE  ;

INSERT INTO  ()
VALUES();

-- REGLAS DE LA BASE:
-- Cada palabra en los campos empieza con mayuscula excepto "numero"
-- Las FOREIGN KEYS son las PRIMARY KEYS que solo pansan al singular excepto metas


-- TABLA: AreasDeTrabajo 
-- Guardar las diferentes areas de tarabajo

CREATE TABLE AreasDeTrabajo (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    Nombre VARCHAR(30)NOT NULL UNIQUE
)ENGINE=InnoDB;

-- TABLA: Estados
-- Guardar la descripcion de las tareas y actividades

CREATE TABLE Estados (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    Descripcion VARCHAR(100) NOT NULL
)ENGINE=InnoDB;

-- TABLA: Modelos
-- Guardar los tipos de modelos que trabaja la empresa

CREATE TABLE Modelos (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    Nombre VARCHAR(30) NOT NULL,
    Descripcion VARCHAR(100)
)ENGINE=InnoDB;

-- TABLA: Piezas
-- Guardar las piezas y la cantidad que tiene la empresa

CREATE TABLE Piezas (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Nombre VARCHAR(30) NOT NULL,
    Stock INT(11) NOT NULL
) ENGINE=InnoDB;


-- TABLA: Materiales
-- Guarda el nombre de los materiales que se usan en la empresa

CREATE TABLE Materiales (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Nombre VARCHAR(30) NOT NULL
) ENGINE=InnoDB;

-- TABLA: Puesto
-- Registra el puesto que ocupa cada empleado en la empresa

CREATE TABLE Puestos (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Nombre VARCHAR(30) NOT NULL
) ENGINE=InnoDB;

-- TABLA: Metas
-- Registra los objetivos que quiere realizar la empresa

CREATE TABLE Metas (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Meta VARCHAR(100) NOT NULL,
    FechaInicio DATE NOT NULL,
    FechaFinal DATE NOT NULL
) ENGINE=InnoDB;


-- TABLA: Actividades
-- Guardar las actividades que se realizan en la empresa

CREATE TABLE Actividades (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT, 
    Descripcion VARCHAR(100),
    Estado INT(11),
    CONSTRAINT Foreign Key (Estado) REFERENCES Estados(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: Empleados
-- Guardar el tiempo que toman las actividades en la bitcora

CREATE TABLE Empleados (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    NombrePila VARCHAR(30) NOT NULL,
    PrimerApellido VARCHAR(30) NOT NULL,
    SegundoApellido VARCHAR(30),
    DirCalle VARCHAR(30) NOT NULL,
    DirNumero VARCHAR(30) NOT NULL,
    DirColonia VARCHAR(30) NOT NULL,
    NumTel VARCHAR(15) NOT NULL,
    Horario TIME,
    JefeArea INT(11),
    AreaDeTrabajo INT(11),
    Puesto INT(11),
    CONSTRAINT Foreign Key (JefeArea) REFERENCES Empleados(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (AreaDeTrabajo) REFERENCES AreasDeTrabajo(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Puesto) REFERENCES Puestos(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: Tareas
-- Guardar la cantidad de tareas y su orden sobre la bitacora
CREATE Table Tareas (
    numero INT(11) PRIMARY KEY AUTO_INCREMENT,
    Orden INT(11),
    AreaDeTrabajo INT(11),
    Estado INT(11),
    CONSTRAINT Foreign Key (AreaDeTrabajo) REFERENCES AreasDeTrabajo(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Estado) REFERENCES Estados(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: Bitacoras
-- Registrar las actividades y metas del empleado

CREATE TABLE Bitacoras (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Titulo VARCHAR(30) NOT NULL,
    Empleado INT(11),
    metas INT(11),
    CONSTRAINT FOREIGN KEY (Empleado) REFERENCES Empleados(Numero) 
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (metas) REFERENCES Metas(Numero) 
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;

-- TABLA: Bit_Tarea
-- Guardar las fechas de las tareas en las bitacoras

CREATE Table Bit_Tarea (
    Bitacora INT(11) NOT NULL,
    Tarea INT(11) NOT NULL,
    FechaInicio DATE NOT NULL,
    FechaFinal DATE,
    HoraInicio TIME NOT NULL,
    HoraFinal TIME,
    CONSTRAINT Foreign Key (Bitacora) REFERENCES Bitacoras(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Tarea) REFERENCES Tareas(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: Bit_Act
-- Guardar el tiempo que toman las actividades en la bitcora

CREATE TABLE Bit_Act (
    Bitacora INT(11) NOT NULL, 
    Actividad INT(11) NOT NULL,
    Tiempo TIME NOT NULL,
    CONSTRAINT Foreign Key (Bitacora) REFERENCES Bitacoras(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Actividad) REFERENCES Actividades(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: AT_Modelos
-- Guardar las cantidades de modelos en areas de trabajo

CREATE TABLE AT_Modelos (
    Area INT(11) NOT NULL, 
    Modelo INT(11) NOT NULL,
    Cantidad INT(255) NOT NULL,
    CONSTRAINT Foreign Key (Area) REFERENCES AreasDeTrabajo(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Modelo) REFERENCES Modelos(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=InnoDB;

-- TABLA: AT_Piezas
-- Guardar la cantidad de piezas en un en el area de trabajo

CREATE TABLE AT_Piezas (
    Area INT(11),
    Pieza INT(11),
    CantidadPieza INT(11) NOT NULL,
    PRIMARY KEY (Area, Pieza),
    CONSTRAINT FOREIGN KEY (Area) REFERENCES AreasDeTrabajo(numero) 
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (Pieza) REFERENCES Piezas(numero) 
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;

-- TABLA: Modelo_Piezas
-- Guardar el tiempo que toman las actividades en la bitcora

CREATE TABLE Modelo_Piezas (
    Modelo INT(11) NOT NULL,
    Pieza INT(11) NOT NULL,
    TotalPiezas INT(11) NOT NULL,
    CONSTRAINT Foreign Key (Modelo) REFERENCES Modelos(numero)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT Foreign Key (Pieza) REFERENCES Piezas(numero)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;

-- TABLA: Piezas_Material
-- Registra el tipo material que ocupa cada pieza

CREATE TABLE Piezas_Material (
    Pieza INT(11),
    Material INT(11),
    FOREIGN KEY (Pieza) REFERENCES Piezas(numero),
    FOREIGN KEY (Material) REFERENCES Materiales(numero)
) ENGINE=InnoDB;

-- TABLA: Errores
-- Registra los errores durante la produccion

CREATE TABLE Errores (
    numero INT(11) PRIMARY KEY AUTO_INCREMENT,
    Descripcion VARCHAR(100) NOT NULL,
    Bitacora INT(11),
    CONSTRAINT FOREIGN KEY (Bitacora) REFERENCES Bitacoras(numero) 
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;

-- TABLA: Soluciones
-- Registrar las soluciones planteadas al error

CREATE TABLE Soluciones (
    numero INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Descripcion VARCHAR(100) NOT NULL,
    Bitacora INT(11),
    `Error` INT(11),
    CONSTRAINT FOREIGN KEY (Bitacora) REFERENCES Bitacoras(numero) 
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (`Error`) REFERENCES Errores(Numero) 
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB;


SELECT CONCAT('DROP TABLE IF EXISTS ', table_name, ';')
FROM information_schema.tables
WHERE table_schema = 'maquila';
